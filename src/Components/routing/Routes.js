import React, { Fragment } from "react";
import { Switch } from "react-router-dom";

// import UserPrivateRoutes from "./UserPrivateRouter";
// import AdminRoutes from './AdminRouter'
// import LayoutAdmin from "../components/Layout/LayoutAdmin";
// import LayoutUser from '../components/Layout/LayoutUser'
import Home  from "../Home/Home";
import PrivateRouter from "./PrivateRouter";
// import { AdminHome } from "../Components/Home/AdminHome";


const Routes = () => {
  return (
                <Fragment>
                  <Switch>
                    {/* <AdminRoutes  path="/admin" component={AdminHome} /> */}
                    <PrivateRouter path="/home" component={Home} /> 
                    {/* <Route component={NotFound} /> */}
                  </Switch>
                </Fragment>
  );
};
export default Routes;
