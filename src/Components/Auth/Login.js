import React, { useState , Fragment } from 'react'
//Route
import {  Redirect } from "react-router-dom";

//Redux
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {login} from '../Redux/action/auth'

const Login = ({ login, isAuthenticated, loading }) => {
    const [formData, setFormData] = useState({
        username: "",
        password: "",
      });

      const { username, password } = formData;

      const onChange = (e) =>
      setFormData({ ...formData, [e.target.name]: e.target.value });
  
    const onSubmit = async (e) => {
      e.preventDefault();
      login(username, password);
    };
  if(isAuthenticated){
      return <Redirect to="/home"/>
  }

    return (
        <Fragment>
                 <div class="flex items-center min-h-screen p-4 bg-gray-100 lg:justify-center">
        <div class="flex flex-col w-12 w-full overflow-hidden bg-white rounded-md shadow-lg max md:flex-row md:flex-1 lg:max-w-screen-lg">
          <div class="lg:flex lg:flex-1 lg:max-w-screen p-4 py-6 text-white bg-red-400 md:w-80 md:flex-1 md:flex-shrink-0 md:flex md:flex-col md:items-center md:justify-evenly">
            <div class="my-3 text-4xl font-bold tracking-wider text-center">
              <a href="#">Affix Tech</a>
            </div>
            <p class="flex flex-col items-center justify-center mt-10 text-center">
              <span>See You The Answer</span>
            </p>

            <div class=" my-8 p-4 max-w-sm mx-auto bg-white rounded-xl shadow-md flex items-center space-x-4 md:justify-center">
              <div class="flex-shrink-0">
                <img
                  class="h-12 w-12"
                  src="/img/logo.svg"
                  alt="ChitChat Logo"
                />
              </div>
              <div>
                <div class="text-xl font-medium text-black">Iphone 13 เปิดตัว</div>
                <p class="text-gray-500  break-words">เมื่อวันที่ 14 กันยายน 2564 ได้เปิดตัว Iphone 13</p>
              </div>
            </div>
            <div class="my-8 p-4 max-w-sm mx-auto bg-white rounded-xl shadow-md flex items-center space-x-4">
              <div class="flex-shrink-0">
                <img
                  class="h-12 w-12"
                  src="/img/logo.svg"
                  alt="ChitChat Logo"
                />
              </div>
              <div>
                <div class="text-xl font-medium text-black">Windows 11 อัพเดทยังไง</div>
                <p class="text-gray-500 break-all">สอบถามว่า Windows 11 อัพเดทยังไงครับ ต้องลงทะเบียนก่อนหรือปล่าว</p>
              </div>
            </div>

            <p class="flex flex-col items-center justify-center mt-10 text-center">
              <span>Don't have an account?</span>
              <a href="#" class="underline">
                Get Started!
              </a>
            </p>
            <p class="mt-6 text-sm text-center text-gray-300">
              Read our{" "}
              <a href="#" class="underline">
                terms
              </a>{" "}
              and{" "}
              <a href="#" class="underline">
                conditions
              </a>
            </p>
          </div>
          <div class="items-center p-5 bg-white md:flex-1">
            <h3 class="my-4 text-2xl font-semibold text-gray-700">
              Account Login
            </h3>
            <form action="#" class="flex flex-col space-y-5">
              <div class="flex flex-col space-y-1">
                <label for="email" class="text-sm font-semibold text-gray-500">
                  Email address
                </label>
                <input
                  type="text"
                  id="text"
                  name="username"
                  value={username}
                  onChange={(e) => onChange(e)}
                  required
                  autofocus
                  class="px-4 py-2 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-red-200"
                />
              </div>
              <div class="flex flex-col space-y-1">
                <div class="flex items-center justify-between">
                  <label
                    for="password"
                    class="text-sm font-semibold text-gray-500"
                  >
                    Password
                  </label>
                  <a
                    href="#"
                    class="text-sm text-red-400 hover:underline focus:text-blue-800"
                  >
                    Forgot Password?
                  </a>
                </div>
                <input
                  type="password"
                  id="password"
                  name="password"
                  value={password}
                  onChange={(e) => onChange(e)}
                  required
                  class="px-4 py-2 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-red-200"
                />
              </div>
              {/*  Check Box Remember
              <div class="flex items-center space-x-2">
              <input
                type="checkbox"
                id="remember"
                class="w-4 h-4 transition duration-300 rounded focus:ring-2 focus:ring-offset-0 focus:outline-none focus:ring-blue-200"
              />
              <label for="remember" class="text-sm font-semibold text-gray-500">Remember me</label>
            </div> */}
              <div className="py-4">
                <button
                  type="submit"
                  onClick={(e) => onSubmit(e)}
                  class="w-full px-4 py-2 text-lg font-semibold text-white transition-colors duration-300 bg-red-400 rounded-md shadow hover:bg-red-400 focus:outline-none focus:ring-red-200 focus:ring-4"
                >
                  Log in
                </button>
              </div>
              <div>
                <button class="w-full px-4 py-2 text-lg font-semibold text-white transition-colors duration-300 bg-red-400 rounded-md shadow hover:bg-red-400 focus:outline-none focus:ring-red-200 focus:ring-4">
                  Sign Up
                </button>
              </div>
              <div class="flex flex-col space-y-5">
                <span class="flex items-center justify-center space-x-2">
                  <span class="h-px bg-gray-400 w-14"></span>
                  <span class="font-normal text-gray-500">or login with</span>
                  <span class="h-px bg-gray-400 w-14"></span>
                </span>
                <div class="flex flex-col space-y-4">
                  <a
                    href="#"
                    class="flex items-center justify-center px-4 py-2 space-x-2 transition-colors duration-300 border border-blue-400 rounded-full group hover:bg-red-400 focus:outline-none"
                  >
                    <span>
                      <img
                        class=" h-5 cursor-pointer"
                        src="https://i.imgur.com/arC60SB.png"
                        alt=""
                      />
                    </span>
                    <span class="text-sm font-medium text-gray-800 group-hover:text-white">
                      Google
                    </span>
                  </a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
        </Fragment>
    )
}

Login.propTypes = {
    login: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool,
  };
  
  const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated,
    loading: state.auth.loading
  });
  
  export default connect(mapStateToProps, { login })(Login);
  