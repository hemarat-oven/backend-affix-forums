import React from 'react'
//React Router
import { Switch, Link } from "react-router-dom";
//Redux
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {logout} from '../Redux/action/auth'

const Home = ({logout,auth:{}}) => {
    return (
        <div>
            <h1>Home Page</h1>
            <button onClick={logout}><Link to="/">Log Out</Link></button>
        </div>
    )
}
Home.propTypes = {
    logout: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
  };
  
  const mapStateToProps = (state) => ({
    auth: state.auth
  });
  export default connect(mapStateToProps, { logout })(Home);
