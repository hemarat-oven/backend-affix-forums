/* eslint-disable no-duplicate-case */
/* eslint-disable no-fallthrough */
/* eslint-disable import/no-anonymous-default-export */
import {
  USER_LOADED,
  USER_LOADFAIL,
  CLEAR_USER,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  LOGOUT,
} from "../action/types";

const initialState = {
  isAuthenticated: false,
  loading: false,
  success: false,
  status: null,
};

export default function (state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case USER_LOADED:
      return {
        ...state,
        isAuthenticated: true,
        loading: false,
        success: payload.success,
        status: payload.status,
      };
    case USER_LOADFAIL:

    case CLEAR_USER:
      return {
        ...state,
        isAuthenticated: false,
        loading: false,
        success: false,
        status: null,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        loading: false,
        success: payload,
        status: payload,
      };
    case LOGIN_FAIL:

    case REGISTER_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        loading: false,
        success: payload,
        status: payload,
      };

    case REGISTER_FAIL:

    case LOGOUT:
      return {
        ...state,
        isAuthenticated: false,
        loading: false,
        success: false,
        status: null,
      };
    default:
      return state;
  }
}
