import {
  USER_LOADED,
  USER_LOADFAIL,
  CLEAR_USER,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  LOGOUT,
} from "../action/types";
import axios from "axios";

export const loadUser = () => async (dispatch) => {
    try {
        const res = await axios.get("/api/auth");
        dispatch({
        type: USER_LOADED,
        payload: res.data,
        })
    } catch (err) {
        dispatch({
        type: USER_LOADFAIL,
        })
    }
};

export const login = (username,password) => async (dispatch) => {
    const config = {
        headers: {
          "Content-Type": "application/json"
        }
      };
    
      const body = JSON.stringify({ username, password });

      try {
          const res = await axios.post("/api/auth/",body,config);
          
          dispatch({
              type:LOGIN_SUCCESS,
              payload: res.data
          })

          dispatch(loadUser())
      } catch (err) {
        dispatch({
            type: LOGIN_FAIL
        });
      }
}


export const logout =()=> async (dispatch) =>{
    try {
        const res = await axios.get("/api/auth/logout");

        dispatch({
            type:LOGOUT,
            payload: res.data
        })

    } catch (err) {
        dispatch({
            type:LOGOUT,
            payload: err
        })
    }
}