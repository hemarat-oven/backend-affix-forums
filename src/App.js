import React, { Fragment } from "react";
import { Route, Switch } from "react-router-dom";
import Login from "./Components/Auth/Login";
import Routes from "./Components/routing/Routes";

//Redux
import {loaduer} from './Components/Redux/action/auth'

const App = () => {
  // useEffect(() => {

  // }, [])
  return (
    <Fragment>
      <Switch>
        <Route exact path="/" component={Login} />
        <Route component={Routes}/>
      </Switch>
    </Fragment>
  );
};

export default App;
